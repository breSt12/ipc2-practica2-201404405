﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
namespace _IPC2_Practica2.Datos
{
    public class Conexion
    {
        private static MySqlConnection conexion = new MySqlConnection(
            "Server=127.0.0.1;Port=3306;Database=practica2;Uid=root;Pwd=admin");

        public static MySqlConnection RecibirConexion()
        {
            return conexion;
        }
    }
}