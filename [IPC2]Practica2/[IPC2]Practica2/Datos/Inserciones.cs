﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
namespace _IPC2_Practica2.Datos
{
    public class Inserciones
    {
        public void agregarProducto(String id, String descripcion, String cantidad, String costo, String iduser, String tipo, String material, String color)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();
            try
            {
                conectar.Open();


                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO PRODUCTO" +
                "(id_producto, descripcion, cantidad, costo, iduser_producto, idtipo_producto, idmat_producto, idcolor_producto)" +
                "values(@id, @desc, @cant,@costo, @iduser,@idtipo,@idmat,@idcolor); ";

                comando.Parameters.AddWithValue("@id", id);
                comando.Parameters.AddWithValue("@desc", descripcion);
                comando.Parameters.AddWithValue("@cant", cantidad);
                comando.Parameters.AddWithValue("@costo", costo);
                comando.Parameters.AddWithValue("@iduser", iduser);
                comando.Parameters.AddWithValue("@idtipo", tipo);
                comando.Parameters.AddWithValue("@idmat", material);
                comando.Parameters.AddWithValue("@idcolor", color);

                try
                {
                    comando.ExecuteNonQuery();
                    Console.WriteLine("Done bro inserted");
                    conectar.Close();


                }
                catch (Exception e)
                {
                    Console.WriteLine("nel mae " + e.ToString());
                }




            }
            catch (Exception e)
            {
                Console.WriteLine("nel mae2" + e.ToString());
            }
        }
        public void agregarUsuario(String id,String usuario,String pwd,string nombre,String apellido)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();
            try
            {
                conectar.Open();


                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "INSERT INTO USUARIO" +
                "(id_usuario,usuario,password,nombre,apellido)" +
                "values(@id, @us, @pwd,@nombre, @apellido); ";

                comando.Parameters.AddWithValue("@id", id);
                comando.Parameters.AddWithValue("@us", usuario);
                comando.Parameters.AddWithValue("@pwd", pwd);
                comando.Parameters.AddWithValue("@nombre", nombre);
                comando.Parameters.AddWithValue("@apellido", apellido);

                try
                {
                    comando.ExecuteNonQuery();
                    Console.WriteLine("Done bro inserted");
                    conectar.Close();


                }
                catch (Exception e)
                {
                    Console.WriteLine("nel mae " + e.ToString());
                }




            }
            catch (Exception e)
            {
                Console.WriteLine("nel mae2" + e.ToString());
            }
        }
        public void ModificarPr(String myid, String id, String descripcion, String cantidad, String costo, String iduser, String tipo, String material, String color)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();
            try
            {
                conectar.Open();


                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "UPDATE PRODUCTO SET " +
                "id_producto=@id,descripcion=@desc,cantidad=@cant,costo=@costo," +
                " iduser_producto = @iduser,idtipo_producto = @idtipo,idmat_producto = @idmat," +
                "idcolor_producto = @idcolor where id_producto = @myid;";

                comando.Parameters.AddWithValue("@id", id);
                comando.Parameters.AddWithValue("@desc", descripcion);
                comando.Parameters.AddWithValue("@cant", cantidad);
                comando.Parameters.AddWithValue("@costo", costo);
                comando.Parameters.AddWithValue("@iduser", iduser);
                comando.Parameters.AddWithValue("@idtipo", tipo);
                comando.Parameters.AddWithValue("@idmat", material);
                comando.Parameters.AddWithValue("@idcolor", color);
                comando.Parameters.AddWithValue("@myid", myid);
                try
                {
                    comando.ExecuteNonQuery();
                    Console.WriteLine("Done bro modified");
                    conectar.Close();


                }
                catch (Exception e)
                {
                    Console.WriteLine("nel mae " + e.ToString());
                }




            }
            catch (Exception e)
            {
                Console.WriteLine("nel mae2" + e.ToString());
            }
        }
        public void ModificarUser(String myid, String id, String user, String pwd, String nombre, String apellido)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();
            try
            {
                conectar.Open();


                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "UPDATE USUARIO SET id_usuario=@id,usuario=@user," +
                    "password=@pwd,nombre=@nombre," +
                    "apellido = @apellido where id_usuario = @myid;";

                comando.Parameters.AddWithValue("@id", id);
                comando.Parameters.AddWithValue("@user", user);
                comando.Parameters.AddWithValue("@pwd", pwd);
                comando.Parameters.AddWithValue("@nombre", nombre);
                comando.Parameters.AddWithValue("@apellido", apellido);
                comando.Parameters.AddWithValue("@myid", myid);
                try
                {
                    comando.ExecuteNonQuery();
                    Console.WriteLine("Done bro modified");
                    conectar.Close();


                }
                catch (Exception e)
                {
                    Console.WriteLine("nel mae " + e.ToString());
                }




            }
            catch (Exception e)
            {
                Console.WriteLine("nel mae2" + e.ToString());
            }
        }
        public void EliminarProducto(String id)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();
            try
            {
                conectar.Open();


                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "DELETE from Producto where id_producto=@id;";

                comando.Parameters.AddWithValue("@id", id);

                try
                {
                    comando.ExecuteNonQuery();
                    Console.WriteLine("Done bro inserted");
                    conectar.Close();


                }
                catch (Exception e)
                {
                    Console.WriteLine("nel mae " + e.ToString());
                }




            }
            catch (Exception e)
            {
                Console.WriteLine("nel mae2" + e.ToString());
            }
        }
        public void EliminarUser(String id)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();
            try
            {
                conectar.Open();


                MySqlCommand comando = new MySqlCommand();
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "DELETE from USUARIO where id_producto=@id;";

                comando.Parameters.AddWithValue("@id", id);

                try
                {
                    comando.ExecuteNonQuery();
                    Console.WriteLine("Done bro inserted");
                    conectar.Close();


                }
                catch (Exception e)
                {
                    Console.WriteLine("nel mae " + e.ToString());
                }




            }
            catch (Exception e)
            {
                Console.WriteLine("nel mae2" + e.ToString());
            }
        }
    }

}