﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MySql.Data.MySqlClient;
namespace _IPC2_Practica2.Datos
{
    public class Consultas
    {
        //Metodo para consultar login
        public String esLogin(String usuario)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "Select password from usuario where usuario=@a;";
                System.Diagnostics.Debug.Write("bro: " + usuario);
                comando.Parameters.AddWithValue("@a", usuario);
               
                
                try
                {
                    String pass=comando.ExecuteScalar().ToString();
                    conectar.Close();
                    System.Diagnostics.Debug.Write("Deh pass: "+pass);
                    return pass;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    System.Diagnostics.Debug.Write("No bro: "+usuario);
                    conectar.Close();
                    return null;
                }
                

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                System.Diagnostics.Debug.Write("No bro: " + usuario);
                return null;
            }
        }
        public List<String> buscarProducto(String id,String tipo,String material,String color)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();
            List<String> informacion=new List<string>();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                if (id.Equals("-1"))
                {
                    comando.CommandText = "SELECT * from Producto where" +
                                            "idtipo_producto=@tip or idmat_producto=@mat or idcolor_producto=@col;";
                    comando.Parameters.AddWithValue("@tip", tipo);
                    comando.Parameters.AddWithValue("@mat", material);
                    comando.Parameters.AddWithValue("@col", color);
                }
                else
                {
                    comando.CommandText = "SELECT * from Producto where id_producto=@a";
                    comando.Parameters.AddWithValue("@a", id);
                }
                
                try
                {
                    MySqlDataReader leer = comando.ExecuteReader();
                    if (leer.Read() == true)
                    {
                        informacion.Add(leer["id_producto"].ToString());
                        informacion.Add(leer["descripcion"].ToString());
                        informacion.Add(leer["cantidad"].ToString());
                        informacion.Add(leer["costo"].ToString());
                        informacion.Add(leer["idtipo_producto"].ToString());
                        informacion.Add(leer["idmat_producto"].ToString());
                        informacion.Add(leer["idcolor_producto"].ToString());

                    }
                    conectar.Close();
                    return informacion;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
     
                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }
        public String[] obtenerProducto(String idProducto)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();
                String[] informacion= new String[9];
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "Select * from producto where id_producto=@a;";
                System.Diagnostics.Debug.Write("bro: " + idProducto);
                comando.Parameters.AddWithValue("@a", idProducto);

                try
                {
                    MySqlDataReader leer = comando.ExecuteReader();
                    if (leer.Read() == true)
                    {
                        System.Diagnostics.Debug.WriteLine("Se encontro correctamente  " + leer[0].ToString());
                        informacion[0] = leer["descripcion"].ToString();
                        informacion[1] = leer["cantidad"].ToString();
                        informacion[2] = leer["costo"].ToString();
                        informacion[3] = leer["idtipo_producto"].ToString();
                        informacion[4] = leer["idmat_producto"].ToString();
                        informacion[5] = leer["idcolor_producto"].ToString();
                    }
                    conectar.Close();
                    return informacion;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    System.Diagnostics.Debug.Write("No bro: " );
                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                System.Diagnostics.Debug.Write("No bro: ");
                return null;
            }
        }
        public String[] obtenerUsuario(String idUser)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();
                String[] informacion = new String[9];
                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "Select * from usuario where id_usuario=@a;";
                comando.Parameters.AddWithValue("@a", idUser);

                try
                {
                    MySqlDataReader leer = comando.ExecuteReader();
                    if (leer.Read() == true)
                    {
                        System.Diagnostics.Debug.WriteLine("Se encontro correctamente  " + leer[0].ToString());
                        informacion[0] = leer["usuario"].ToString();
                        informacion[1] = leer["password"].ToString();
                        informacion[2] = leer["nombre"].ToString();
                        informacion[3] = leer["apellido"].ToString();
                    }
                    conectar.Close();
                    return informacion;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    System.Diagnostics.Debug.Write("No bro: ");
                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                System.Diagnostics.Debug.Write("No bro: ");
                return null;
            }
        }

        public String idUsuario(String usuario)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "Select id_usuario from usuario where usuario=@a;";
                System.Diagnostics.Debug.Write("my bro user : " + usuario);
                comando.Parameters.AddWithValue("@a", usuario);


                try
                {
                    String id = comando.ExecuteScalar().ToString();
                    conectar.Close();
                    System.Diagnostics.Debug.WriteLine("Deh id user: " + id);
                    return id;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    System.Diagnostics.Debug.WriteLine("No id user bro: " + usuario);
                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                System.Diagnostics.Debug.WriteLine("No id user bro: " + usuario);
                return null;
            }
        }
        public String nombreTipo(String tipo)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "Select nombre from tipo where id_tipo=@a;";
                System.Diagnostics.Debug.Write("bro : " + tipo);
                comando.Parameters.AddWithValue("@a", tipo);


                try
                {
                    String id = comando.ExecuteScalar().ToString();
                    conectar.Close();
                    System.Diagnostics.Debug.Write("Deh id tipo: " + id);
                    return id;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    System.Diagnostics.Debug.Write("No id tipo bro: " + tipo);
                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                System.Diagnostics.Debug.Write("No id Tipo bro: " + tipo);
                return null;
            }
        }
        public String idTipo(String tipo)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "Select id_tipo from tipo where nombre=@a;";
                System.Diagnostics.Debug.Write("bro : " + tipo);
                comando.Parameters.AddWithValue("@a", tipo);


                try
                {
                    String id = comando.ExecuteScalar().ToString();
                    conectar.Close();
                    System.Diagnostics.Debug.Write("Deh id tipo: " + id);
                    return id;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    System.Diagnostics.Debug.Write("No id tipo bro: " + tipo);
                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                System.Diagnostics.Debug.Write("No id Tipo bro: " + tipo);
                return null;
            }
        }
        public String idMaterial(String material)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "Select id_material from material where nombre=@a;";
                System.Diagnostics.Debug.Write("bro: " + material);
                comando.Parameters.AddWithValue("@a", material);


                try
                {
                    String id = comando.ExecuteScalar().ToString();
                    conectar.Close();
                    System.Diagnostics.Debug.WriteLine("Deh mat id: " + id);
                    return id;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    System.Diagnostics.Debug.WriteLine("No id mat bro: " + material);
                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                System.Diagnostics.Debug.WriteLine("No id material bro: " + material);
                return null;
            }
        }
        public String nombreMaterial(String material)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "Select nombre from material where id_material=@a;";
                System.Diagnostics.Debug.Write("bro: " + material);
                comando.Parameters.AddWithValue("@a", material);


                try
                {
                    String id = comando.ExecuteScalar().ToString();
                    conectar.Close();
                    System.Diagnostics.Debug.WriteLine("Deh mat id: " + id);
                    return id;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    System.Diagnostics.Debug.WriteLine("No id mat bro: " + material);
                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                System.Diagnostics.Debug.WriteLine("No id material bro: " + material);
                return null;
            }
        }

        public String idColor(String color)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "Select id_color from color where nombre=@a;";
                System.Diagnostics.Debug.Write("bro: " + color);
                comando.Parameters.AddWithValue("@a", color);


                try
                {
                    String id = comando.ExecuteScalar().ToString();
                    conectar.Close();
                    System.Diagnostics.Debug.WriteLine("Deh id color: " + id);
                    return id;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    System.Diagnostics.Debug.WriteLine("No id color bro: " + color);
                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                System.Diagnostics.Debug.WriteLine("No id color bro: " + color);
                return null;
            }
        }
        public String nombreColor(String color)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "Select nombre from color where id_color=@a;";
                System.Diagnostics.Debug.Write("bro: " + color);
                comando.Parameters.AddWithValue("@a", color);


                try
                {
                    String id = comando.ExecuteScalar().ToString();
                    conectar.Close();
                    System.Diagnostics.Debug.WriteLine("Deh id color: " + id);
                    return id;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                    System.Diagnostics.Debug.WriteLine("No id color bro: " + color);
                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                System.Diagnostics.Debug.WriteLine("No id color bro: " + color);
                return null;
            }
        }
        public String idProducto()
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "SELECT id_producto from producto ORDER by id_producto DESC limit 1;";


                try
                {
                    String id = comando.ExecuteScalar().ToString();
                    conectar.Close();
                    System.Diagnostics.Debug.WriteLine("Producto id: " + id);
                    return id;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());

                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());

                return null;
            }
        }

        public String lastidUser()
        {
            MySqlConnection conectar = Conexion.RecibirConexion();

            try
            {
                conectar.Open();

                MySqlCommand comando = new MySqlCommand();

                comando.Connection = conectar;

                comando.CommandType = System.Data.CommandType.Text;
                comando.CommandText = "SELECT id_usuario from usuario ORDER by id_usuario DESC limit 1;";


                try
                {
                    String id = comando.ExecuteScalar().ToString();
                    conectar.Close();
                    System.Diagnostics.Debug.WriteLine("Producto id: " + id);
                    return id;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());

                    conectar.Close();
                    return null;
                }


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());

                return null;
            }
        }
    }
}