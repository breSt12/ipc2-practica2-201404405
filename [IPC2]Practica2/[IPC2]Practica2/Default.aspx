﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_IPC2_Practica2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

          <!-- Top content -->
        <div class="top-content">
        	
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2 text">
                            <h1>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                                Practica 2</h1>
                            <div class="description">
                            	<p>
	                            	&nbsp;</p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3 form-box">
                        	<div class="form-top">
                        		<div class="form-top-left">
                        			<h3>Login</h3>
                            		<p>Ingresa tu usuario y contraseña</p>
                        		</div>
                        		<div class="form-top-right">
                        			<i class="fa fa-lock"></i>
                        		</div>
                            </div>
                            <div class="form-bottom">
			                    <form role="form" action="" method="post" class="login-form">
			                    	<div class="form-group">
			                    		<label class="sr-only" for="form-username">Username</label>&nbsp;
                                        <asp:TextBox ID="TextBox1" runat="server" Height="45px" Width="851px"></asp:TextBox>
			                        </div>
			                        <div class="form-group">
			                        	<label class="sr-only" for="form-password">Password</label>&nbsp;
                                        <asp:TextBox ID="TextBox2" runat="server" Height="45px" Width="851px" 
                                            TextMode="Password"></asp:TextBox>
			                        </div>
			                    </form>
		                        <asp:Button ID="Button1" runat="server" BorderStyle="Solid" 
                                    CssClass="btn-danger" OnClick="Button1_Click" Text="Log in!" />
		                    </div>
                        </div>
                    </div>
                    <div class="row">
                    </div>
                </div>
            </div>
            
        </div>


</asp:Content>
