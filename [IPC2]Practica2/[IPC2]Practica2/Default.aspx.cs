﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _IPC2_Practica2.Datos;
namespace _IPC2_Practica2
{
    
    public partial class _Default : Page
    {
        public static String user;
        protected void Page_Load(object sender, EventArgs e)
        {
            user = "";
        }
        public String getUser()
        {
            return user;
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String usuario = Convert.ToString(TextBox1.Text);
            
            String pass = Convert.ToString(TextBox2.Text);
            Consultas login = new Consultas();

            try
            {
                String realPass = login.esLogin(usuario);
                System.Diagnostics.Debug.Write(realPass);
                if (realPass.Equals(pass))
                {
                    System.Diagnostics.Debug.Write("Usuario y Contraseña Correcto");
                    user = usuario;
                    Response.Write("<script>window.alert('Welcome User');</script>");
                    Response.Redirect("/Paginas/Opciones.aspx", true);
                    
                }
                else
                {
                    System.Diagnostics.Debug.Write("Usuario y Contraseña Incorrecto");
                    Response.Write("<script>window.alert('Contraseña Incorrecta');</script>");
                }
            }
            catch
            {
                System.Diagnostics.Debug.Write("El Usuario NO EXISTE!");
                Response.Write("<script>window.alert('Usuario inexsistente');</script>");
            }
        }
    }
}