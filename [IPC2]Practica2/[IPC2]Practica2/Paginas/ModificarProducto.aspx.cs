﻿using _IPC2_Practica2.Datos;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _IPC2_Practica2.Paginas
{
    public partial class ModificarProducto : System.Web.UI.Page
    {
        Boolean yes = true;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (DropDownList1.Items.Count==0)
            {
                MySqlConnection conectar = Conexion.RecibirConexion();
                try
                {
                    
                        String cmd = "SELECT id_producto from PRODUCTO;";

                        conectar.Open();
                        MySqlCommand comando = new MySqlCommand();
                        comando.Connection = conectar;
                        comando.CommandType = System.Data.CommandType.Text;
                        comando.CommandText = cmd;

                        try
                        {
                            MySqlDataReader reader;
                            reader = comando.ExecuteReader();

                            while (reader.Read())
                            {

                                    String value = reader.GetString("id_producto");
                                    DropDownList1.Items.Add(value);
                                



                            }
                            conectar.Close();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.ToString());
                            conectar.Close();
                        }
                    
                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            String valor = DropDownList1.SelectedValue;
            Console.WriteLine("Valor: " + valor);
            String[] info = new string[9];
            Consultas consult = new Consultas();
            info = consult.obtenerProducto(valor);
            Console.WriteLine("Valor : " + DropDownList2.SelectedValue);
            TextBox1.Text = info[0];
            TextBox2.Text = info[1];
            TextBox3.Text = info[2];
            tbATipo.Text = info[3];
            tbAMat.Text = info[4];
            tbAColor.Text = info[5];
            //Response.Redirect("/Paginas/ModificarProducto.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String valor = DropDownList1.SelectedValue;
            Console.WriteLine("Valor: " + valor);
            String[] info = new string[9];
            Consultas consult = new Consultas();
            info = consult.obtenerProducto(valor);
            Console.WriteLine("Valor : " + DropDownList2.SelectedValue);
            TextBox1.Text = info[0];
            TextBox2.Text = info[1];
            TextBox3.Text = info[2];
            tbATipo.Text = consult.nombreTipo(info[3]);
            tbAMat.Text = consult.nombreMaterial(info[4]);
            tbAColor.Text = consult.nombreColor(info[5]);
            //Response.Redirect("/Paginas/ModificarProducto.aspx");
            if (DropDownList2.Items.Count == 0)
            {
                Llenar();
            }
            
        }
        public void Llenar()
        {
            MySqlConnection conectar = Conexion.RecibirConexion();
            yes = false;
            try
            {
                for (int i = 0; i <= 2; i++)
                {
                    String cmd="";
                    if (i == 0)
                    {
                        cmd = "SELECT nombre from TIPO;";
                    }
                    else if (i == 1)
                    {
                        cmd = "SELECT nombre from MATERIAL;";
                    }
                    else if (i == 2)
                    {
                        cmd = "SELECT nombre from COLOR;";
                    }

                    conectar.Open();
                    MySqlCommand comando = new MySqlCommand();
                    comando.Connection = conectar;
                    comando.CommandType = System.Data.CommandType.Text;
                    comando.CommandText = cmd;

                    try
                    {
                        MySqlDataReader reader;
                        reader = comando.ExecuteReader();

                        while (reader.Read())
                        {
                            if (i == 0)
                            {
                                String value = reader.GetString("nombre");
                                DropDownList2.Items.Add(value);
                                
                            }
                            else if (i == 1)
                            {
                                String value = reader.GetString("nombre");
                                DropDownList3.Items.Add(value);
                            }
                            else if (i == 2)
                            {
                                String value = reader.GetString("nombre");
                                DropDownList4.Items.Add(value);
                            }



                        }
                        conectar.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        conectar.Close();
                    }
                }
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        
    }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            Console.WriteLine("Valorro+ "+DropDownList2.SelectedValue);
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            String descripcion = TextBox1.Text.ToString();
            String cantidad = TextBox2.Text.ToString();
            String costo = TextBox3.Text.ToString();
            String Tipo = DropDownList2.SelectedValue;
            String material = DropDownList3.SelectedValue;
            String color = DropDownList4.SelectedValue;
            String myid = DropDownList1.SelectedValue;
            Consultas consult = new Consultas();
            String myt=consult.idTipo(Tipo);
            String mym=consult.idMaterial(material);
            String myc=consult.idColor(color);
            Inserciones cs = new Inserciones();
            String user = "1";
            System.Diagnostics.Debug.WriteLine("MD: " + myid + "-" + descripcion + "-" + cantidad + "-" + costo + "-" + user + "-" + myt + "-" + mym + "-" + myc);
            cs.ModificarPr(myid, myid, descripcion, cantidad, costo, user, myt, mym, myc);

            Response.Write("<script>window.alert('Modificado');</script>");
            // Response.Redirect("/Paginas/ModificarProducto.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Paginas/Opciones.aspx");
        }
    }
}