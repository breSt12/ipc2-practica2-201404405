﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Opciones.aspx.cs" Inherits="_IPC2_Practica2.Paginas.Opciones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
    &nbsp;</p>
<p>
    &nbsp;</p>
<p>
    <table class="nav-justified">
        <tr>
            <td style="width: 461px">
    <asp:Button ID="btCargaMasiva" runat="server" CssClass="btn-info focus" 
        Text="Carga Masiva" Width="168px" />
    <asp:Button ID="btAgregar" runat="server" CssClass="btn-info focus" 
        Text="Agregar Producto" Width="168px" OnClick="btAgregar_Click" Height="26px" />
            </td>
            <td class="modal-sm" style="width: 175px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 461px; height: 52px">
    <asp:Button ID="btBuscar" runat="server" CssClass="btn-info focus" 
        Text="Buscar Producto" Width="168px" />
    <asp:Button ID="btEliminar" runat="server" CssClass="btn-info focus" 
        Text="Eliminar Producto" Width="168px" OnClick="btEliminar_Click" />
    <asp:Button ID="btLista" runat="server" CssClass="btn-info focus" 
        Text="Lista Productos" Width="168px" OnClick="btLista_Click" />
    <asp:Button ID="btModificar" runat="server" CssClass="btn-info focus" 
        Text="Modificar Producto" Width="168px" OnClick="btModificar_Click" />
            </td>
            <td class="modal-sm" style="width: 175px; height: 52px"></td>
            <td style="height: 52px"></td>
        </tr>
        <tr>
            <td style="width: 461px">
                <asp:Button ID="Button1" runat="server" CssClass="btn-info focus" 
                    OnClick="Button1_Click" Text="Crear Usuario" Width="168px" />
                <asp:Button ID="Button2" runat="server" CssClass="btn-info focus" 
                    OnClick="Button2_Click" Text="Modificar Usuario" Width="168px" />
                <asp:Button ID="Button3" runat="server" CssClass="btn-info focus" 
                    OnClick="Button3_Click" Text="Eliminar Usuario" Width="168px" />
            </td>
            <td class="modal-sm" style="width: 175px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</p>
<p>
    &nbsp;</p>
</asp:Content>
