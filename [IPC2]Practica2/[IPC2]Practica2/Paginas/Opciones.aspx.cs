﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _IPC2_Practica2.Paginas
{
    public partial class Opciones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btAgregar_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Paginas/Agregar.aspx");
        }

        protected void btEliminar_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Paginas/EliminarPr.aspx");
        }

        protected void btLista_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Paginas/ListaProductos.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Paginas/CrearUser.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Paginas/ModifyUser.aspx");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Paginas/EliminarUser.aspx");
        }

        protected void btModificar_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Paginas/ModificarProducto.aspx");
        }
    }
}