﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EliminarPr.aspx.cs" Inherits="_IPC2_Practica2.Paginas.EliminarPr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
    </p>
    <p>
        <table style="width:100%;">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px"></td>
                <td style="height: 20px; font-size: x-large; font-weight: bold; font-style: normal;">
                    ELIMINAR PRODUCTO</td>
                <td style="height: 20px"></td>
            </tr>
            <tr>
                <td style="height: 20px; font-size: large; font-weight: bold; font-style: normal;">
                    id</td>
                <td>
                    <asp:DropDownList ID="DropDownList1" runat="server" 
                        OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px; font-size: large; font-weight: bold; font-style: normal;">&nbsp;</td>
                <td>
                    <asp:Button ID="Button1" runat="server" CssClass="btn-info" 
                        OnClick="Button1_Click" Text="Cargar" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px; font-size: large; font-weight: bold; font-style: normal;">
                    Descripcion</td>
                <td>
                    <asp:TextBox ID="TextBox1" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px; font-size: large; font-weight: bold; font-style: normal;">
                    Cantidad</td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px; font-size: large; font-weight: bold; font-style: normal;">
                    Costo</td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px; font-size: large; font-weight: bold; font-style: normal;">
                    Tipo</td>
                <td>
                    <asp:TextBox ID="TextBox4" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px; font-size: large; font-weight: bold; font-style: normal;">
                    Material</td>
                <td>
                    <asp:TextBox ID="TextBox5" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px; font-size: large; font-weight: bold; font-style: normal;">
                    Color</td>
                <td>
                    <asp:TextBox ID="TextBox6" runat="server" Enabled="False"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px; font-size: large; font-weight: bold; font-style: normal;">&nbsp;</td>
                <td>
                    <asp:Button ID="Button2" runat="server" CssClass="btn-info" 
                        OnClick="Button2_Click" Text="Eliminar" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="height: 20px; font-size: large; font-weight: bold; font-style: normal;">&nbsp;</td>
                <td>
                    <asp:Button ID="Button3" runat="server" CssClass="btn-info" 
                        OnClick="Button3_Click" Text="Volver" Width="75px" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </p>
</asp:Content>
