﻿using _IPC2_Practica2.Datos;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
namespace _IPC2_Practica2.Paginas
{
    public partial class EliminarPr : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (DropDownList1.Items.Count == 0)
            {
                MySqlConnection conectar = Conexion.RecibirConexion();
                try
                {

                    String cmd = "SELECT id_producto from PRODUCTO;";

                    conectar.Open();
                    MySqlCommand comando = new MySqlCommand();
                    comando.Connection = conectar;
                    comando.CommandType = System.Data.CommandType.Text;
                    comando.CommandText = cmd;

                    try
                    {
                        MySqlDataReader reader;
                        reader = comando.ExecuteReader();

                        while (reader.Read())
                        {

                            String value = reader.GetString("id_producto");
                            DropDownList1.Items.Add(value);




                        }
                        conectar.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        conectar.Close();
                    }

                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String valor = DropDownList1.SelectedValue;
            Console.WriteLine("Valor: " + valor);
            String[] info = new string[9];
            Consultas consult = new Consultas();
            info = consult.obtenerProducto(valor);
            TextBox1.Text = info[0];
            TextBox2.Text = info[1];
            TextBox3.Text = info[2];
            TextBox4.Text = info[3];
            TextBox5.Text = info[4];
            TextBox6.Text = info[5];

            //Response.Redirect("/Paginas/ModificarProducto.aspx");
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Inserciones ins = new Inserciones();
            ins.EliminarProducto(DropDownList1.SelectedValue);
            Response.Write("<script>window.alert('Eliminado');</script>");
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Paginas/Opciones.aspx");
        }
    }
}