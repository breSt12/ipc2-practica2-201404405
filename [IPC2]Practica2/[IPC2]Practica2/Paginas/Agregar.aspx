﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Agregar.aspx.cs" Inherits="_IPC2_Practica2.Paginas.Agregar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        &nbsp;</p>
    <p style="font-family: ARIal, Helvetica, sans-serif; font-size: large; font-weight: bold; font-style: normal; font-variant: normal;">
        AGREGAR PRODUCTO<table style="width:100%;">
            <tr>
                <td style="width: 135px; height: 31px; font-size: large; font-weight: bold; font-style: normal;">
                    Tipo</td>
                <td style="height: 31px">
        <asp:DropDownList ID="DropDownList1" runat="server" 
            OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
        </asp:DropDownList>
                </td>
                <td style="height: 31px"></td>
            </tr>
            <tr>
                <td style="width: 135px; height: 31px; font-size: large; font-weight: bold; font-style: normal;">
                    Material</td>
                <td style="height: 31px"> <asp:DropDownList ID="DropDownList2" runat="server">
        </asp:DropDownList>
                </td>
                <td style="height: 31px"></td>
            </tr>
            <tr>
                <td style="width: 135px; height: 31px; font-size: large; font-weight: bold; font-style: normal;">
                    Color&nbsp;&nbsp;</td>
                <td style="height: 31px"> <asp:DropDownList ID="DropDownList3" runat="server">
        </asp:DropDownList>
                </td>
                <td style="height: 31px"></td>
            </tr>
            <tr>
                <td style="width: 135px; height: 31px; font-size: large; font-weight: bold; font-style: normal;">
                    Descripcion</td>
                <td style="height: 31px">
        <asp:TextBox ID="TextBox1" runat="server" Width="200px"></asp:TextBox>
                </td>
                <td style="height: 31px"></td>
            </tr>
            <tr>
                <td style="width: 135px; height: 31px; font-size: large; font-weight: bold; font-style: normal;">
                    Cantidad</td>
                <td style="height: 31px">
        <asp:TextBox ID="TextBox2" runat="server" OnTextChanged="TextBox2_TextChanged" 
            TextMode="Number"></asp:TextBox>
                </td>
                <td style="height: 31px"></td>
            </tr>
            <tr>
                <td style="width: 135px; height: 31px; font-size: large; font-weight: bold; font-style: normal;">
                    Costo</td>
                <td>
        <asp:TextBox ID="TextBox3" runat="server" TextMode="Number"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="width: 135px">&nbsp;</td>
                <td>
        <asp:Button ID="Button1" runat="server" CssClass="btn-info" 
            OnClick="Button1_Click" Text="Agregar" Width="107px" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </p>
    </asp:Content>
