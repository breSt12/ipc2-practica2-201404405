﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using _IPC2_Practica2.Datos;
using MySql.Data.MySqlClient;
namespace _IPC2_Practica2.Paginas
{
    public partial class Agregar : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            MySqlConnection conectar = Conexion.RecibirConexion();
            try
            {
                for (int i = 0; i < 3; i++)
                {
                    String cmd = "SELECT nombre from TIPO;";
                    if (i == 1)
                    {
                        cmd = "SELECT nombre from MATERIAL;";
                    }
                    else if (i == 2)
                    {
                        cmd = "SELECT nombre from COLOR;";
                    }

                    

                    conectar.Open();
                    MySqlCommand comando = new MySqlCommand();
                    comando.Connection = conectar;
                    comando.CommandType = System.Data.CommandType.Text;
                    comando.CommandText =cmd;

                    try
                    {
                        MySqlDataReader reader;
                        reader = comando.ExecuteReader();

                        while (reader.Read())
                        {
                            String value = reader.GetString("nombre");
                            if (i == 0)
                            {
                                DropDownList1.Items.Add(value);
                            }
                            if (i == 1)
                            {
                                DropDownList2.Items.Add(value);
                            }
                            else if (i == 2)
                            {
                                DropDownList3.Items.Add(value);
                            }
                            

                        }
                        conectar.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        conectar.Close();
                    }
                   
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
           
           
            
    
        }


        protected void Button1_Click(object sender, EventArgs e)
        {
            String tipo = DropDownList1.Text.ToString();
            String material = DropDownList2.Text.ToString();
            String color = DropDownList3.Text.ToString();
            String desc = TextBox1.Text.ToString();
            String cantidad = TextBox2.Text.ToString();
            String costo = TextBox3.Text.ToString();
            String iduser;
            String idtipo;
            String idmat;
            String idcolor;
            _Default dd = new _Default();
            
            Consultas consult = new Consultas();

            try
            {
                String pen = dd.getUser();
                Console.WriteLine("Pen " + pen);
                iduser =consult.idUsuario(pen);
                idtipo = consult.idTipo(tipo);
                idmat = consult.idMaterial(material);
                idcolor = consult.idColor(color);
                int idproducto = Convert.ToInt32(consult.idProducto());
                idproducto++;
                Inserciones insert = new Inserciones();
                
                insert.agregarProducto(idproducto.ToString(), desc, cantidad, costo, iduser, idtipo, idmat, idcolor);
                Response.Write("<script>window.alert('Done');</script>");
                Response.Redirect("/Paginas/Agregar.aspx");
            }
            catch
            {
                System.Diagnostics.Debug.Write("Incorrecto");
                Response.Write("<script>window.alert('Incorrecto');</script>");
            }
        }

        protected void TextBox2_TextChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownList1_SelectedIndexChanged1(object sender, EventArgs e)
        {

        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}