﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ModifyUser.aspx.cs" Inherits="_IPC2_Practica2.Paginas.ModifyUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
    <br />
</p>
<p>
    <table style="width:100%;">
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td style="font-size: x-large; font-weight: bold">Modificar Usuario</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold">Id Usuario</td>
            <td>
                <asp:DropDownList ID="DropDownList1" runat="server">
                </asp:DropDownList>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold">&nbsp;</td>
            <td>
                <asp:Button ID="Button2" runat="server" CssClass="btn-info" 
                    OnClick="Button2_Click" Text="Cargar" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold">Usuario</td>
            <td>
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold">Pass</td>
            <td>
                <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold">Nombre</td>
            <td>
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold">Apellido</td>
            <td>
                <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold">&nbsp;</td>
            <td>
                <asp:Button ID="Button1" runat="server" CssClass="btn-info" 
                    OnClick="Button1_Click" Text="Modificar" />
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold">&nbsp;</td>
            <td>
                <asp:Button ID="Button3" runat="server" CssClass="btn-info" 
                    OnClick="Button3_Click" Text="Volver" Width="84px" />
            </td>
            <td>&nbsp;</td>
        </tr>
    </table>
</p>
</asp:Content>
