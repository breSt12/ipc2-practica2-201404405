﻿using _IPC2_Practica2.Datos;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _IPC2_Practica2.Paginas
{
    public partial class ModifyUser : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (DropDownList1.Items.Count == 0)
            {
                MySqlConnection conectar = Conexion.RecibirConexion();
                try
                {

                    String cmd = "SELECT id_usuario from USUARIO;";

                    conectar.Open();
                    MySqlCommand comando = new MySqlCommand();
                    comando.Connection = conectar;
                    comando.CommandType = System.Data.CommandType.Text;
                    comando.CommandText = cmd;

                    try
                    {
                        MySqlDataReader reader;
                        reader = comando.ExecuteReader();

                        while (reader.Read())
                        {

                            String value = reader.GetString("id_usuario");
                            DropDownList1.Items.Add(value);




                        }
                        conectar.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                        conectar.Close();
                    }

                }

                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                }
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            String valor = DropDownList1.SelectedValue;
            Console.WriteLine("Valor: " + valor);
            String[] info = new string[9];
            Consultas consult = new Consultas();
            info = consult.obtenerUsuario(valor);
            TextBox1.Text = info[0];
            TextBox2.Text = info[1];
            TextBox3.Text = info[2];
            TextBox4.Text = info[3];



        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Paginas/Opciones.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {


            Inserciones ins = new Inserciones();
            String id = DropDownList1.SelectedValue;
            String user = TextBox1.Text.ToString();
            String pass = TextBox2.Text.ToString();
            String nombre = TextBox3.Text.ToString();
            String apellido = TextBox4.Text.ToString();
            ins.ModificarUser(id, id, user, pass, nombre, apellido);
            Response.Write("<script>window.alert('Modificado');</script>");
        }
    }
}