﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaProductos.aspx.cs" Inherits="_IPC2_Practica2.Paginas.ListaProductos" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
    </p>
    <p>
        <table style="width:100%;">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="font-family: arial, Helvetica, sans-serif; font-size: large; font-weight: bold;">
                    Lista Productos</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>
                    <asp:GridView ID="GridView1" runat="server" DataSourceID="SqlDataSource1">
                    </asp:GridView>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" 
                        ConnectionString="<%$ ConnectionStrings:practica2ConnectionString3 %>" 
                        ProviderName="<%$ ConnectionStrings:practica2ConnectionString3.ProviderName %>" 
                        SelectCommand="SELECT id_producto, descripcion, cantidad, costo, iduser_producto, idtipo_producto, idmat_producto, idcolor_producto FROM producto">
                    </asp:SqlDataSource>
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </p>
</asp:Content>
