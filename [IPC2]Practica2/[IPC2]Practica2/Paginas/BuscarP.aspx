﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="BuscarP.aspx.cs" Inherits="_IPC2_Practica2.Paginas.BuscarP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <p>
        <br />
    </p>
    <p>
        <table style="width:100%;">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td style="font-size: x-large; font-weight: bold">Buscar Producto</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="font-size: large; font-weight: bold; height: 25px;">id</td>
                <td style="height: 25px">
                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                </td>
                <td style="height: 25px"></td>
            </tr>
            <tr>
                <td style="font-size: large; font-weight: bold">Tipo</td>
                <td>
                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="font-size: large; font-weight: bold">Material</td>
                <td>
                    <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="font-size: large; font-weight: bold">Color</td>
                <td>
                    <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="font-size: large; font-weight: bold">&nbsp;</td>
                <td>
                    <asp:Button ID="Button1" runat="server" CssClass="btn-info" 
                        OnClick="Button1_Click" Text="Buscar" />
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="font-size: large; font-weight: bold">&nbsp;</td>
                <td>
                    <asp:GridView ID="GridView1" runat="server">
                        <Columns>
                            <asp:BoundField HeaderText="Descripcion" />
                            <asp:BoundField HeaderText="Cantidad" />
                            <asp:BoundField HeaderText="Costo" />
                            <asp:BoundField HeaderText="Id Producto" />
                            <asp:BoundField HeaderText="Id Tipo" />
                            <asp:BoundField HeaderText="id Material" />
                            <asp:BoundField HeaderText="Id Color" />
                        </Columns>
                    </asp:GridView>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td style="font-size: large; font-weight: bold">&nbsp;</td>
                <td>
                    <asp:Button ID="Button2" runat="server" CssClass="btn-info" 
                        OnClick="Button2_Click" Text="Volver" Width="64px" />
                </td>
                <td>&nbsp;</td>
            </tr>
        </table>
    </p>
</asp:Content>
