﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ModificarProducto.aspx.cs" Inherits="_IPC2_Practica2.Paginas.ModificarProducto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <table style="width:100%;">
        <tr>
            <td>&nbsp;</td>
            <td class="modal-sm" style="width: 285px">&nbsp;</td>
            <td style="width: 129px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td class="modal-sm" 
                style="font-size: x-large; font-weight: bold; font-style: normal; width: 285px;">
                Modificar un Producto</td>
            <td style="width: 129px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">
                Producto</td>
            <td style="height: 34px; width: 285px">
                <asp:DropDownList ID="DropDownList1" runat="server" 
                    OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td style="height: 34px; width: 129px"></td>
            <td style="height: 34px">&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: x-large; font-weight: bold; font-style: normal; height: 26px;">
            </td>
            <td style="height: 26px; width: 285px">
                <asp:Button ID="Button1" runat="server" CssClass="btn-info" 
                    OnClick="Button1_Click" Text="Cargar Informacion" />
            </td>
            <td style="height: 26px; width: 129px"></td>
            <td style="height: 26px">&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">
                Descripcion</td>
            <td class="modal-sm" style="width: 285px">
                <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            </td>
            <td style="width: 129px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">
                Cantidad</td>
            <td style="height: 34px; width: 285px">
                <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            </td>
            <td style="height: 34px; width: 129px"></td>
            <td style="height: 34px">&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">
                Costo</td>
            <td class="modal-sm" style="width: 285px">
                <asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            </td>
            <td style="width: 129px">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">
                Tipo</td>
            <td style="height: 34px; width: 285px">
                <asp:DropDownList ID="DropDownList2" runat="server" 
                    OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">
                Actual</td>
            <td style="height: 34px">
                <asp:TextBox ID="tbATipo" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">
                Material</td>
            <td class="modal-sm" style="width: 285px">
                <asp:DropDownList ID="DropDownList3" runat="server">
                </asp:DropDownList>
            </td>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">
                Actual</td>
            <td>
                <asp:TextBox ID="tbAMat" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">
                Color</td>
            <td class="modal-sm" style="width: 285px">
                <asp:DropDownList ID="DropDownList4" runat="server">
                </asp:DropDownList>
            </td>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">
                Actual</td>
            <td>
                <asp:TextBox ID="tbAColor" runat="server" Enabled="False"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">&nbsp;</td>
            <td class="modal-sm" style="width: 285px">
                <asp:Button ID="Button2" runat="server" CssClass="btn-info" 
                    OnClick="Button2_Click" Text="Modificar" Width="97px" />
            </td>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">&nbsp;</td>
            <td class="modal-sm" style="width: 285px">
                <asp:Button ID="Button3" runat="server" CssClass="btn-info" Text="Volver" 
                    Width="97px" OnClick="Button3_Click" />
            </td>
            <td style="font-size: large; font-weight: bold; font-style: normal; height: 34px;">&nbsp;</td>
            <td>&nbsp;</td>
        </tr>
    </table>
</asp:Content>
