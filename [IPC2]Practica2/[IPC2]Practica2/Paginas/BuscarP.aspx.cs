﻿using _IPC2_Practica2.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _IPC2_Practica2.Paginas
{
    public partial class BuscarP : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            GridView1.AutoGenerateColumns = false;
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect("/Paginas/Opciones.aspx");
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            String id, tipo, mat, col;
            if (TextBox1.Text.ToString().Count() > 0)
            {
                id = TextBox1.Text.ToString();
            }
            else
            {
                id = "-1";
            }
            if (TextBox2.Text.ToString().Count() > 0)
            {
                tipo = TextBox1.Text.ToString();
            }
            else
            {
                tipo = "-1";
            }
            if (TextBox3.Text.ToString().Count() > 0)
            {
                mat = TextBox1.Text.ToString();
            }
            else
            {
                mat = "-1";
            }
            if (TextBox4.Text.ToString().Count() > 0)
            {
                col = TextBox1.Text.ToString();
            }
            else
            {
                col = "-1";
            }
            Consultas consult = new Consultas();
            List<String> info=consult.buscarProducto(id, tipo, mat, col);

        }
    }
}