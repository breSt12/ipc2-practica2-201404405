CREATE DATABASE Practica2
USE PRACTICA2

CREATE TABLE USUARIO(
		id_usuario int not null primary key,
		usuario varchar(30) not null,
		password varchar(20) not null,
		nombre varchar(30),
		apellido varchar(30))
		
CREATE TABLE TIPO(
		id_tipo int not null primary key,
		nombre varchar(30))
		
CREATE TABLE MATERIAL(
		id_material int not null primary key,
		nombre varchar(30))
		
CREATE TABLE Color(
		id_color int not null primary key,
		nombre varchar(30))

CREATE TABLE Producto(
		id_producto int not null primary key,
		descripcion varchar(50),
		cantidad int, 
		costo float
		)
ALTER TABLE Producto add iduser_producto int not null;
ALTER TABLE Producto add idtipo_producto int not null; 
ALTER TABLE Producto add idmat_producto int not null;
ALTER TABLE Producto add idcolor_producto int not null;
ALTER TABLE Producto
			add constraint FOREIGN KEY(iduser_producto) references usuario(id_usuario);
ALTER TABLE Producto
			add constraint FOREIGN KEY(idtipo_producto) references tipo(id_tipo);
ALTER TABLE Producto
			add constraint FOREIGN KEY(idmat_producto) references material(id_material);
ALTER TABLE Producto
			add constraint FOREIGN KEY(idcolor_producto) references Color(id_color);